﻿using Newtonsoft.Json;
using Shop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Shop.Services
{
    public static class ProductService
    {
        public static string GetJsonProductList(IQueryable<Product> products)
        {
            List<Product> productsList;

            //Products to list
            productsList = new List<Product>();
            productsList = products.ToList();

            //Add url to image file name
            productsList.ForEach(delegate (Product p)
            {
                p.Images.ToList<Image>().ForEach(delegate (Image i)
                {
                    i.file_name = ImageService.GetFileUrl(i.file_name);
                }); 
            });

            //Convert to json
            var json = JsonConvert.SerializeObject(productsList,
                       new JsonSerializerSettings
                       {
                           ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                           Formatting = Formatting.None,
                       });
            
            return json;
        }

        public static string GetJsonProduct(Product product)
        {
            //Add url to image file name
            product.Images.ToList<Image>().ForEach(delegate (Image i)
            {
                i.file_name = ImageService.GetFileUrl(i.file_name);
            });

            //Convert to json
            var json = JsonConvert.SerializeObject(product,
                       new JsonSerializerSettings
                       {
                           ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                           Formatting = Formatting.None,
                       });

            return json;
        }
    }
}