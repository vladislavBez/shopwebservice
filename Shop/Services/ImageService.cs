﻿using Shop.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shop.Services
{
    public static class ImageService
    {
        public static String GetRandomName(HttpPostedFileBase image)
        {
            return String.Format("{0}{1}", Guid.NewGuid(), TypeService.GetExtension(image.ContentType));
        }

        public static String GetFilePath(String fileName)
        {
            return String.Format("{0}{1}{2}", GlobalConstants.PATH_TO_PRODUCTS_FOLDER, "/", fileName);
        }

        public static String GetFileUrl(String fileName)
        {
            return String.Format("{0}{1}{2}", GlobalConstants.URL_TO_PRODUCTS_FOLDER, "/", fileName);
        }

        public static void RemoveFile(String path)
        {
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
            }
        }
    }
}