﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shop.Services
{
    public static class TypeService
    {
        public static string GetExtension(string mimeType)
        {
            string result;
            RegistryKey key;
            object value;

            key = Registry.ClassesRoot.OpenSubKey(@"MIME\Database\Content Type\" + mimeType, false);
            value = key.GetValue("Extension", null);
            result = value.ToString();

            return result;
        }
    }
}