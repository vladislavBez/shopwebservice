﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shop.Constants
{
    public static class GlobalConstants
    {
        public const string PATH_TO_PRODUCTS_FOLDER = "~/Uploads/products";
        public const string ROOT_URL = "http://localhost:50735";
        public const string URL_TO_PRODUCTS_FOLDER = ROOT_URL + "/Uploads/products";

    }
}