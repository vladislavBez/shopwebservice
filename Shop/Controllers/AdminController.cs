﻿using Shop.Attributes;
using Shop.Constants;
using Shop.Models;
using Shop.Services;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Shop.Controllers
{
    public class AdminController : Controller
    {
        private DBShopEntities db = new DBShopEntities();        

        public ActionResult GetProductsList(string search, int? priceFrom, int? priceTo, string sortOrder)
        {
            IQueryable<Product> products;
            const string NAME = "NAME";
            const string NAME_DESC = "NAME_DESC";
            const string PRICE = "PRICE";
            const string PRICE_DESC = "PRICE_DESC";
            const string ID = "ID";
            const string ID_DESC = "ID_DESC";
            //Validate request data
            if ((search == null) || (priceFrom == null) || (priceTo == null))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //Get all products regardless of price
            if (priceTo == 0)
            {
                products = db.Products.Where(p => p.name.Contains(search)).Include(i => i.Images);
            }
            //Get all products with price
            else
            {
                products = db.Products.Where(p => p.name.Contains(search))
                .Where(p => p.price >= priceFrom)
                .Where(p => p.price <= priceTo);
            }

            //Sort by order
            switch (sortOrder)
            {
                case NAME:
                    products = products.OrderBy(p => p.name);
                    break;
                case NAME_DESC:
                    products = products.OrderByDescending(p => p.name);
                    break;
                case PRICE:
                    products = products.OrderBy(p => p.price);
                    break;
                case PRICE_DESC:
                    products = products.OrderByDescending(p => p.price);
                    break;
                case ID:
                    products = products.OrderBy(p => p.id);
                    break;
                case ID_DESC:
                    products = products.OrderByDescending(p => p.id);
                    break;
                default:
                    break;
            }
            
            return Json(ProductService.GetJsonProductList(products), JsonRequestBehavior.AllowGet);
        }

        public ActionResult getProductDetails(int? id)
        {
            Product product;

            //Validate request data
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //Get product
            product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }

            return Json(ProductService.GetJsonProduct(product), JsonRequestBehavior.AllowGet);
        }

        public ActionResult PostSetProduct(string name, int? price, HttpPostedFileBase image)
        {
            Product product;
            Image imageProduct;
            String fileName = string.Empty;
            
            //Validate request data
            if ((name == null) || (price == null) || (image == null))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            product = new Product();
            imageProduct = new Image();

            //It is new product. It will be save in DB 
            product.name = name;
            product.price = price;
            db.Products.Add(product);
            db.SaveChanges();

            fileName = ImageService.GetRandomName(image);

            imageProduct.Product = product;
            imageProduct.file_name = fileName;
            db.Images.Add(imageProduct);
            db.SaveChanges();

            image.SaveAs(Server.MapPath(ImageService.GetFilePath(fileName)));
            
            return Json(ProductService.GetJsonProduct(product), JsonRequestBehavior.AllowGet);
        }

        public ActionResult PostEditProduct(int? id, string name, int? price, HttpPostedFileBase image)
        {
            Product product;
            Image imageProduct;
            String fileName = string.Empty;

            //Validate request data
            if ((id == null) || (name == null) || (price == null))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            product = new Product();
            imageProduct = new Image();
            
            //It is existing product. It will be edit in DB 
            product.id = (int)id;
            product.name = name;
            product.price = price;
            db.Entry(product).State = EntityState.Modified;
            db.SaveChanges();

            if (image != null) {
                fileName = ImageService.GetRandomName(image);

                imageProduct = db.Images.FirstOrDefault(p => p.Product == product);
                ImageService.RemoveFile(Server.MapPath(ImageService.GetFilePath(imageProduct.file_name)));
                imageProduct.file_name = fileName;
                db.Entry(imageProduct).State = EntityState.Modified;
                db.SaveChanges();

                image.SaveAs(Server.MapPath(ImageService.GetFilePath(fileName)));
            }
            
            return Json(ProductService.GetJsonProduct(product), JsonRequestBehavior.AllowGet);
        }

    }
}